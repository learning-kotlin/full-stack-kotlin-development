allprojects {
    group = "org.elu.leaning.kotlin.fullstack"
    version = "1.0"
    repositories {
        jcenter()
    }
}

plugins {
     base
}

dependencies {
  subprojects.forEach {
    archives(it)
  }
}
