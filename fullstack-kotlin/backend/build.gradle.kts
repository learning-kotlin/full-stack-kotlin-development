import org.gradle.jvm.tasks.Jar
import org.gradle.kotlin.dsl.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:2.0.0.RELEASE")
    }
}

plugins {
    kotlin("jvm") version "1.2.30"
    id("io.spring.dependency-management") version "1.0.3.RELEASE"
    id("org.jetbrains.kotlin.plugin.spring") version embeddedKotlinVersion
}

apply {
    plugin("org.springframework.boot")
}

dependencies {
    compile("org.springframework.boot:spring-boot-starter")
    compile(kotlin("stdlib-jre8"))
    compile(kotlin("reflect"))
}

val project = mapOf(
        name to "backend"
)

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}
